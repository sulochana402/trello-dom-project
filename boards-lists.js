document.addEventListener("DOMContentLoaded", () => {
    const urlParams = new URLSearchParams(window.location.search);
    const boardId = urlParams.get('boardId');
    if (boardId) {
        fetchLists(boardId);
        fetchBoardName(boardId);
        // Add event listener to the reveal create list form button
        document.getElementById('revealCreateListFormBtn').addEventListener('click', () => {
            toggleCreateListForm();
        });

        // Add event listener to the create list button inside the pop-up form
        document.getElementById('createListBtn').addEventListener('click', () => {
            const listName = document.getElementById('newListName').value.trim();
            if (listName) {
                createList(boardId, listName);
            }
        });

        // Add event listener to the create card button inside the pop-up form
        document.getElementById('createCardBtn').addEventListener('click', () => {
            const listId = document.getElementById('popupCard').getAttribute('data-list-id');
            const cardName = document.getElementById('newCardName').value.trim();
            if (cardName) {
                createCard(listId, cardName);
            }
        });

        // Add event listener to the close popup button for the card form
        document.getElementById('closePopup').addEventListener('click', () => {
            toggleCreateCardForm();
        });

        // Add event listener to the close popup button for the list form
        document.getElementById('closePopupBtn').addEventListener('click', () => {
            toggleCreateListForm();
        });
    }
});

const apiKey = "8a4740d9065fe8c56882abe40d8db2ee";
const token = "ATTA7e81032976e353ef371b41f60ba683237f716cff8c051f9c4720b9cebebf1ddd91F71D67";

function fetchLists(boardId) {
    fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${token}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch lists');
            }
            return response.json();
        })
        .then(data => {
            displayLists(data);
        })
        .catch(error => {
            console.error('Error fetching lists:', error);
        });
}


function fetchBoardName(boardId) {
    fetch(`https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${token}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch board');
            }
            return response.json();
        })
        .then(board => {
            document.getElementById('boardName').textContent = board.name;
        })
        .catch(error => {
            console.error('Error fetching board:', error);
        });
}
function displayLists(lists) {
    const listsContainer = document.getElementById('listsContainer');
    listsContainer.innerHTML = '';

    lists.forEach(list => {
        addListToDisplay(list);
        fetchCardsForList(list.id);
    });
}

function createList(boardId, listName) {
    fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${token}`, {
        method: 'POST'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create list');
        }
        return response.json();
    })
    .then(newList => {
        addListToDisplay(newList);
        document.getElementById('newListName').value = '';
        toggleCreateListForm();
    })
    .catch(error => {
        console.error('Error creating list:', error);
    });
}

function toggleCreateListForm() {
    const popup = document.getElementById('popup');
    popup.classList.toggle('hidden');

    if (!popup.classList.contains('hidden')) {
        document.getElementById('newListName').focus();
    }
}

function addListToDisplay(list) {
    const listsContainer = document.getElementById('listsContainer');
    const listContainer = document.createElement('div');
    listContainer.classList.add("p-4", "rounded-md", "shadow-md", "md:w-60", "h-max", "md:mx-4", "mb-4", "relative", "bg-black");
    listContainer.id = `list-${list.id}`;
    listContainer.style.backgroundColor = 'black';

    const listHeader = createListHeader(list);
    const cardsContainer = createCardsContainer(list);

    listContainer.appendChild(listHeader);
    listContainer.appendChild(cardsContainer);

    listsContainer.appendChild(listContainer);
}

function createListHeader(list) {
    const listHeader = document.createElement('div');
    listHeader.classList.add("flex", "justify-between", "items-center");

    const listName = document.createElement('span');
    listName.textContent = list.name;
    listName.classList.add("text-white");

    const deleteBtn = createDeleteButton(list.id);

    listHeader.appendChild(listName);
    listHeader.appendChild(deleteBtn);

    return listHeader;
}

function fetchCardsForList(listId) {
    fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${token}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to fetch cards for list');
            }
            return response.json();
        })
        .then(cards => {
            displayCardsForList(listId, cards);
        })
        .catch(error => {
            console.error('Error fetching cards for list:', error);
        });
}

function displayCardsForList(listId, cards) {
    const cardsContainer = document.querySelector(`#cardsContainer-${listId} .card-items-container`);

    cards.forEach(card => {
        const cardItem = createCardElement(card.id, card.name); 
        cardsContainer.appendChild(cardItem);
    });
}

function toggleCreateCardForm(listId) {
    const popupCard = document.getElementById('popupCard');
    popupCard.classList.toggle('hidden');

    if (!popupCard.classList.contains('hidden')) {
        document.getElementById('newCardName').focus();
        popupCard.setAttribute('data-list-id', listId); 
    } else {
        popupCard.removeAttribute('data-list-id');  
    }
}

function createCard(listId, cardName) {
    fetch(`https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${apiKey}&token=${token}`, {
        method: 'POST'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create card');
        }
        return response.json();
    })
    .then(newCard => {
        const cardsContainer = document.querySelector(`#cardsContainer-${listId} .card-items-container`);
        const cardItem = createCardElement(newCard.id, newCard.name);
        cardsContainer.appendChild(cardItem);

        toggleCreateCardForm();

        document.getElementById('newCardName').value = '';
    })
    .catch(error => {
        console.error('Error creating card:', error);
    });
}

function createDeleteButton(listId) {
    const deleteBtn = document.createElement('button');
    deleteBtn.classList.add('text-white', 'ml-1');
    deleteBtn.innerHTML = '<i class="fas fa-times"></i>';
    deleteBtn.addEventListener('click', (event) => {
        event.stopPropagation();
        deleteList(listId);
    });
    return deleteBtn;
}

function deleteList(listId) {
    fetch(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${token}`, {
        method: 'PUT'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to close list');
        }
        document.getElementById(`list-${listId}`).remove();
    })
    .catch(error => {
        console.error('Error closing list:', error);
    });
}

function createCardsContainer(list) {
    const cardsContainer = document.createElement('div');
    cardsContainer.id = `cardsContainer-${list.id}`;
    cardsContainer.classList.add("mt-4");

    const cardItemsContainer = document.createElement('div');
    cardItemsContainer.classList.add("card-items-container");

    const createCardBtn = document.createElement('button');
    createCardBtn.classList.add("text-white", "rounded-md", "px-4", "py-2");
    createCardBtn.style.marginBottom = '0';  
    createCardBtn.addEventListener('click', () => {
        toggleCreateCardForm(list.id);
    });

    const plusIcon = document.createElement('span');
    plusIcon.classList.add("mr-1");
    plusIcon.innerHTML = "&#43;";

    createCardBtn.appendChild(plusIcon);
    createCardBtn.appendChild(document.createTextNode(" Add a card"));

    cardsContainer.appendChild(cardItemsContainer);
    cardsContainer.appendChild(createCardBtn);

    return cardsContainer;
}

function createCardElement(cardId, cardName) {
    const cardItem = document.createElement('div');
    cardItem.classList.add("bg-slate-600", "p-2", "rounded-md", "md:w-56", "h-24", "mb-2", "relative", "flex", "items-center", "justify-between");
    cardItem.style.backgroundColor = "rgb(71 85 105)";
    cardItem.id = `card-${cardId}`;

    const cardText = document.createElement('span');
    cardText.textContent = cardName;
    cardText.classList.add("block", "text-white", "flex-grow");

    const deleteBtn = createCardDeleteButton(cardId);

    deleteBtn.addEventListener('click', (event) => {
        event.stopPropagation();
        deleteCard(cardId);
    });

    cardItem.appendChild(cardText);
    cardItem.appendChild(deleteBtn);

    return cardItem;
}

function createCardDeleteButton(cardId) {
    const deleteBtn = document.createElement('button');
    deleteBtn.classList.add('text-white');
    deleteBtn.innerHTML = '<i class="fas fa-times"></i>';
    deleteBtn.addEventListener('click', (event) => {
        event.stopPropagation();
        deleteCard(cardId);
    });
    return deleteBtn;
}

function deleteCard(cardId) {
    fetch(`https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`, {
        method: 'DELETE'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to delete card');
        }
        document.getElementById(`card-${cardId}`).remove();
    })
    .catch(error => {
        console.error('Error deleting card:', error);
    });
}
