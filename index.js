const apiKey = "8a4740d9065fe8c56882abe40d8db2ee";
const token = "ATTA7e81032976e353ef371b41f60ba683237f716cff8c051f9c4720b9cebebf1ddd91F71D67";

function fetchBoards() {
  return fetch(
    `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`
  )
    .then((response) => {
      if (!response.ok) {
        throw new Error("Failed to fetch boards");
      }
      return response.json();
    })
    .catch((error) => {
      console.error("Error fetching boards:", error);
      throw error;
    });
}

function displayBoards(boards) {
  const boardsContainer = document.getElementById("boards");
  boards.forEach((board) => {
    const boardElement = document.createElement("button");
    boardElement.classList.add(
      "bg-sky-700",
      "p-4",
      "rounded-md",
      "shadow-md",
      "md:w-80",
      "h-34",
      "md:mx-4",
      "mb-4"
    );
    boardElement.textContent = board.name;
    boardElement.addEventListener("click", () => {
      navigateToBoardListsPage(board.id);
    });
    boardsContainer.appendChild(boardElement);
  });
}

function navigateToBoardListsPage(boardId) {
  const url = `boards-lists.html?boardId=${boardId}`;
  window.location.href = url;
}

function handlePopupButtonClick() {
  const popup = document.getElementById("popup");
  const errorMsg = document.getElementById("errorMsg");

  popup.classList.toggle("hidden");
  errorMsg.classList.add("hidden");
}

function handleSubmitButtonClick(event) {
  event.preventDefault();

  const inputValue = popupInput.value.trim();
  if (inputValue !== "") {
    createBoard(inputValue)
      .then((newBoard) => {
        displayBoards([newBoard]);
        popup.classList.add("hidden");
        popupInput.value = "";
        errorMsg.classList.add("hidden");
      })
      .catch((error) => {
        errorMsg.classList.remove("hidden");
      });
  } else {
    errorMsg.classList.remove("hidden");
  }
}

function hidePopupAndClearInput() {
  const popup = document.getElementById("popup");
  const popupInput = document.getElementById("popupInput");
  const errorMsg = document.getElementById("errorMsg");

  popup.classList.add("hidden");
  popupInput.value = "";
  errorMsg.classList.add("hidden");
}

function fetchAndDisplayBoards() {
  fetchBoards()
    .then((boards) => {
      displayBoards(boards);
    })
    .catch((error) => {
      console.error("Error fetching and displaying boards:", error);
    });
}

document.addEventListener("DOMContentLoaded", () => {
  const popupButton = document.getElementById("popupButton");
  const submitBtn = document.getElementById("submitBtn");

  popupButton.addEventListener("click", handlePopupButtonClick);
  submitBtn.addEventListener("click", handleSubmitButtonClick);

  fetchAndDisplayBoards();
});
