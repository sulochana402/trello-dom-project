# Trello Board Lists

This is a simple web application that allows users to view and manage boards and lists similar to Trello.

### Features

- **View Boards:** Display a list of boards fetched from the Trello API.
- **Create Board:** Users can create a new board by entering a title.
- **View Lists:** Display lists within a selected board.
- **Create List:** Users can create a new list within a board.
- **Delete List:** Users can delete a list within a board
- **View Cards:** Display cards within each list.
- **Delete Card:** Delete a card within a list

### Technologies Used

- HTML
- CSS (Tailwind CSS)
- JavaScript DOM
